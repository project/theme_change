<?php

namespace Drupal\theme_change;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining an Theme Change entity.
 */
interface ThemeChangeInterface extends ConfigEntityInterface {
  
}
